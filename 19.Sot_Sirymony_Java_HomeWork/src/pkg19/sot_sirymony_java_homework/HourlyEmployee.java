package pkg19.sot_sirymony_java_homework;

import java.util.Scanner;

public class HourlyEmployee extends StaffMember {

    private int hourWorked;
    private double rate;

    /**
     *
     */
    

    public HourlyEmployee(int id, String name, String address,int hourWorked,double rate) {
        super(id, name, address);
        this.hourWorked=hourWorked;
        this.rate=rate;
    }
    void output_hourlyemployee()
    {
        System.out.println("id:"+get_id());
        System.out.println("address:"+get_address());
        System.out.println("hourWorked:"+get_HourWorked());
        System.out.println("rate:"+get_rate());
    }

    HourlyEmployee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  

   
   
    
    public void set_hourWorked(int hourWorked){
        this.hourWorked=hourWorked;
    }
    public int get_HourWorked(){
        return hourWorked;
    }
    public void set_rate(double rate){
        this.rate=rate;
    }
    public double get_rate(){
        return rate;
    }

   
        void input_HourlyEmployee()
    {
        Scanner src = new Scanner(System.in); 
         /**input id**/
          System.out.println("=>Enter HourWorked:");
          hourWorked = src.nextInt();
         /**input name**/
         System.out.println("=>Enter rate:");
          rate=src.nextDouble();
    }

    public String toString(){
        return  "Id      : "+ id+"\n"+
                "Name    : "+ name+"\n"+
                "Address : "+address+"\n"+
                "Hours   : "+ hourWorked+"\n"+
                "Rate    : "+ rate+"\n"+
                "Payment : "+ pay()+"\n";
    }

    @Override
    double pay() {
        return hourWorked*rate;
    }
}