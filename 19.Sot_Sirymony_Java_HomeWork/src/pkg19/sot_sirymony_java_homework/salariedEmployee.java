package pkg19.sot_sirymony_java_homework;

import java.util.Scanner;

public class salariedEmployee extends StaffMember{

    private double salary, bonus;

    public salariedEmployee(int id, String name, String address) {
        super(id, name, address);
    }

    salariedEmployee() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
   
    public void set_salary(double salary){
        this.salary=salary;
    }
    public double get_salary(){
        return salary;
    }
    public void set_bonus(double bonus){
        this.bonus=bonus;
    }
    public double get_bonus(){
        return bonus;
    }

    public salariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id,name,address);
        this.salary=salary;
        this.bonus=bonus;
    }
    void output_salariedEmployee()
    {
        System.out.println("id:"+get_id());
        System.out.println("name:"+get_name());
        System.out.println("address:"+get_address());
        System.out.println("salary:"+get_salary());
        System.out.println("bonus:"+get_bonus());
    }
        void input_salaryiedEmployee()
    {
        Scanner src = new Scanner(System.in); 
         /**input id**/
          System.out.println("=>Enter Salary:");
          salary = src.nextDouble();
         /**input name**/
         System.out.println("=>Enter Bonus:");
          bonus=src.nextDouble();
         
    }

    
    @Override
     public String toString(){
        return  "Id      : "+ id+"\n"+
                "Name    : "+ name+"\n"+
                "Address : "+address+"\n"+
                "Salary  : "+ salary+"\n"+
                "Bonus   : "+bonus+"\n"+
                "Payment : "+pay()+"\n";
    }

    @Override
    double pay() {
        return salary+bonus;
    }

}
