/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg19.sot_sirymony_java_homework;

import java.util.Scanner;

public abstract class StaffMember {
    protected int id;
    protected String name,address;
    
    public void set_id(int id){
        this.id=id;
    }
    public int get_id(){
        return id;
    }
    public void set_name(String name){
        this.name = name;
    }
    public String get_name(){
        return name;
    }
    public void set_address(String address){
        this.address=address;
    }
    public String get_address(){
        return address;
    }
    StaffMember()
    {
        
    }
    public StaffMember(int id,String name,String address){
        this.id=id;
        this.name=name;
        this.address=address;
        
    }  
      void input_StaffMember()
    {
         Scanner src = new Scanner(System.in); 
         /**input id**/
          System.out.println("=>Enter  Id:");
          id = src.nextInt();
          src.nextLine();
         /**input name**/
         System.out.println("=>Enter name:");
         name=src.nextLine();
         /*input address*/
         System.out.println("=>Enter address:");
         address=src.nextLine();
    }

    @Override
    public String toString(){
        return  "Id      : "+ id+"\n"+
                "Name    : "+ name+"\n"+
                "Address : "+address+"\n"+
                "Thank You! \n";
    }
    abstract double pay();
}
